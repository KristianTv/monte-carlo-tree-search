import numpy as np
from NeuralNetPolicy import NeuralNet
from MCTS import MCTS
class MonteCarlo:
    def __init__(self):
        self.NN = NeuralNet()
        self.actual_games = 1
        self.search_games = 1

        self.save_interval = 50
        self.mcts = MCTS()


    def choose_move(self, D):            # TODO Choose best of D distribution
        return 1


    def general_algorithm(self,do_action, get_state, reset, is_finished):

        state = None

        # Clear Replay Buffer (RBUF)
        RBUF = []
        
        # Randomly initialize params (weights and biases) of ANET
        self.NN.initialize_params()
        
        for ag in range(self.actual_games):
            # Initialize the actual game board to an empty board
            reset()

            # S_init <- starting_board_state
            state_init = get_state()

            # Initialize the Monte Carlo Tree (MCT) to a single root, which represents s_init
            self.mcts.initialize_tree(state_init)

            while not is_finished():    # While Ba not in a final state   (actual game board)
                # Initialize Monte Carlo game board (B_mc) to same state as root
                self.mcts.initialize_game_board()

                # For g in numer_search_games
                for sg in range(self.search_games):
                    # Use tree policy Pt to search from root to a leaf (L) of MCT. Update Bmc with each move
                    self.mcts.search()
                    pass

                # Distribution of visit counts in MCT along all arcs emanating from root
                dist_visit_counts = 1

                # Add case (root, D) to RBUF
                RBUF.append(["root"], dist_visit_counts)

                # Choose actual move (a*) based on D
                a_prime = self.choose_move(dist_visit_counts)

                # Perform a* on root to produce
                state_prime = do_action(a_prime)

                # Update actual board to state_prime (SHouldnt it happen in do action?)

                # In MCT, retain subtree rooted as s* discard everything else
                root = self.mcts.get_root(state_prime)

            self.NN.train(*RBUF)


            if ag % self.save_interval == 0:
                # Save ANETs current parameters for later use in tournament play
                self.NN.save_params(ag)







