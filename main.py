import numpy as np

from MCTS import MCTS
from NeuralNetPolicy import NeuralNetPolicy
from NIM import NIM

if __name__ == '__main__':
    """
    Monte carlo tree search will later be operated by the RL framework.
    This is for testing purposes before implementing MCTS with RL
    """
    num_stones_NIM = 40
    max_takeable_pieces = 5

    nim_actual_game = NIM(num_stones_NIM, max_takeable_pieces)
    nim_simulation = NIM(num_stones_NIM, max_takeable_pieces)

    nim_nn_policy = NeuralNetPolicy(state_size=1, output_size=max_takeable_pieces)  # One input node for leftover pieces # TODO Pieces left + max? Or nah

    mcts = MCTS(nim_simulation.get_state(), nim_simulation, nim_nn_policy, M=50)     # State is beginning state, environment is for simulation
    mcts.tree_search()
