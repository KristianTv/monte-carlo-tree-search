import numpy as np

from silence_tensorflow import silence_tensorflow
silence_tensorflow()

import tensorflow as tf
from tensorflow.keras import layers
import keras.backend as K


class NeuralNetPolicy:

    def __init__(self, state_size, output_size):
        input_shape = (state_size, )

        self.model = tf.keras.Sequential()
        self.model.add(layers.Dense(8, activation="relu", input_shape=input_shape))    # Specifying input shape, builds the network automatically
        self.model.add(layers.Dense(8, activation="relu"))
        self.model.add(layers.Dense(8, activation="relu"))       # TODO How many outputs on HEX board? Should be all of them (the size of the board, but just exclude the ones that are not possible AFTER the prediction
        self.model.add(layers.Dense(output_size, activation="sigmoid"))       # TODO How many outputs on HEX board? Should be all of them (the size of the board, but just exclude the ones that are not possible AFTER the prediction
        self.model.compile(optimizer='adam', loss='mse')

    def predict(self, x):
        return self.model.predict(x)

    def train(self, samples, targets):
        self.model.fit(samples, targets)

    def initialize_params(self):
        for lyr in self.model.layers:
            self.init_layer(lyr)

    def init_layer(layer):
        session = K.get_session()
        weights_initializer = tf.variables_initializer(layer.weights)
        session.run(weights_initializer)


    def save_params(self, epochs):
        self.model.save(str(epochs))

    def load_params(self, epochs):
        self.model = keras.models.load_model(str(epochs))