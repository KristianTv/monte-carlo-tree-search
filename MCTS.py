from copy import deepcopy
from random import choice
import numpy as np

class MCTS:
    def __init__(self, state, environment, default_policy, M):
        self.visit_count = {} # Dict for visit counts for each node
        self.rewards = {}    # Total rewards
        self.epsilon = 1     # Exploration
        self.environment = environment
        self.M = M           # Number of simulations, should start low, end high (shorter simulations)
        # TODO Have a instance of the game object to traverse during the tree search
        """
        When there has been performed M simulations. The Q values should all be updated.
        The RL then chooses the best action that MCTS has come with. Which is the highest Q value from the first layers
        under the root. When this move is done, the root becomes the subtree of the chosen node to explore further.
        
        The result from the MCTS is to return an appropriate best action, which is performed outside of this. 
        The MCTS therefore needs a "sample environment" to run simulations in. 
        """
        self.root = Node(None, None)        # Save initial state as root node without specific parent or move
        self.root_state = deepcopy(state)   # For safety reasons lol
        self.subtree = None                 # Buffer variable for storing subtree ? # TODO redundant?

        self.tree_policy = dict()       # Tree policy for evaluating the next traversal of non simulation steps
        self.default_policy = default_policy        # Neural net policy for evaluating the appropriate action during simulation

        # Helper variables
        self.node_count = 0     # No idea lol? # TODO dafoq
        self.n_rollouts = 0     # Number of rollouts done

    def tree_search(self):
        # TODO M number of simulations should be handled here. Run M simulations and backprops before returning best
        # TODO action returned from selection
        # From root state (R) use the tree policy to choose the next pre-existing child node
        print(self.selection())
        if not self.expansion():        # The game is finished, returning false
            return "The leaf node reached here/the action from this leaf node"
        for _ in range(self.M):
            self.simulation()   # Otherwise run simulations and backprops N times
            self.backprop()


    def selection(self):
        # Select a node to perform a rollout from
        # So go from root to a leaf node

        state = deepcopy(self.root_state)
        print(state)
        node = self.root
        # Continue until leaf node (where children are not yet expanded (children = 0)
        while len(node.get_children()):
            # Select the child node with largest value to descend down
            children = node.get_children()
            maxVal = 0                          # " TREE POLICY  "
            for child in children:              # " TREE POLICY  "
                if child.get_value() > maxVal:  # " TREE POLICY  "
                    maxVal = child.get_value    # " TREE POLICY  "
            np.shuffle(children)       # Shuffle for random behavior if there are multiple with highest value
            node = next(child for child in children if child.get_value() == maxVal)       # Returns the next item from the iterator

            self.environment.do_action(node.move)     # Update the simulation environment with this selected node action

            # If the selected child node has not been explored yet, select it before expanding others
            if not node.visited:
                return node, state



    def expansion(self, parent_node):
        """
        Function for expanding a node, generate its children and add them too tree.
        """
        child_nodes = []

        # TODO Check if game is finished here?
        if self.environment.is_final():         # If the game is finished, don't expand and wrap up search
            return False

        actions = self.environment.get_actions_from_state(self.state)     # Get the possible actions from the current state
        for action in actions:
            child_nodes.append(Node(parent_node, action))

        parent_node.add_children(child_nodes)
        return True     # Signal to continue


    def simulation(self):  # Rollout
        """
        From the initial leaf state of the game, perform rollouts
        according to the tree policy (TODO make tree policy? Or random moves)
        TODO Simulation should be done M times (as set by the user.) PER MOVE (Can be done in parent func)
        TODO ROllout should follow the default policy.
        TODO
        """
        # Due to static environment, we can just remove the action after performing them
        actions = self.environment.get_actions()
        state = self.environment.get_state()

        while self.environment.finished() == 0:  # Hasent ended yet
            # Using the default policy to retrieve possibilities for all actions, then filter and select best action
            probabilities = self.default_policy.predict(state)
            action = self.environment.filter_select_legal_move(probabilities)

            new_state = self.environment.do_action(action)   # Perform action and get new state
            actions.remove(action)          # Remove from legal moves

        # Return the win state
        return self.environment.finished()

    def backprop(self, node):
        """
        Update entire tree with information gathered at game end. (outcome)
        """

        reward = self.environment.finished()
        alternating_reward = 1
        while node is not None:     # Run upward in the tree
            node.visited += 1
            node.reward += reward * alternating_reward  # The MIN player receives 0 and winning player 1
            node = node.parent
            alternating_reward = 0 if alternating_reward else 1


class Node:
    def __init__(self, parent, move):
        self.move = move        # The move that was taken to get to this node
        self.parent = parent    # Parent of this node
        self.children = []      # List of children
        self.visited = 0        # Number of times this node was visited
        self.reward = 0         # Average reward wins and losses from this position
        self.winner = 0         # If the node is a winner node this is set to 1

    def get_children(self):
        return self.children

    def add_children(self, children: dict):     # Child dictionary where the key is the move
        for child in children:
            self.children.append(child)

    def get_value(self, epsilon):
        """
        Function for calculating the UCT value of this node.
        Calculation is done relative to its parent.
        """
        # Implement upper confidence bound (UCT) TODO Diff between N(s) and N(s, a)
        c = 1
        U = c * (np.sqrt(np.log(self.visited)) / (1 + self.visited))  # Last part is very uncertain

        if not self.visited:    # If the node hasnt been visited yet, set the value as infinity to promote exploration
            return 0 if epsilon == 0 else np.inf    # Dont explore if exploration term has reached 0
        else:   #   Q(s,a) + exploration term  (last one is exploration term, first one is evaluation.)
            return self.reward / self.visited + epsilon * np.sqrt(2 * np.log(self.parent.visited) / self.visited)  # exploitation + exploration

        """
        Alternative for use with critic: 
        
        mixing_param = 0.5
        critic_eval = critic(leaf_state)
        value = (1-mixing_param) * critic_eval + mixing_param * self.reward  # reward  = result for the node (-1, 0, 1)
        """


