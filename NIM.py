import numpy as np

class NIM:

    def __init__(self, N, K):
        """
        Chinese game where players take turn removing pieces, and the player who
        removes the last piece is the winner. By changing the values of N and K,
        you can produce many variations of the game, some of which are harder than others.
        """
        self.min_take = 1
        self.K = K       # Maximum that a player can take at once
        self.N = N       # Number of pieces on the board

        self.player = 1  # Player number

    def do_action(self, action):
        """
        Perform the requested action
        :param action: Integer of the number of pieces to take.
        :return: Nothing
        """

        self.N -= action

        # Alternating players
        if self.player == 1:
            self.player = 2
        else:
            self.player = 1


    def get_actions(self):
        if self.K >= self.N:
            return np.arange(self.min_take, self.N+1)
        else:
            return np.arange(self.min_take, self.K+1)

    def get_actions_from_state(self, state):
        """
        Infers the possible actions from a given state of N
        :param state:
        :return:
        """
        N = state
        if self.K >= N:
            return np.arange(self.min_take, N+1)
        else:
            return np.arange(self.min_take, self.K+1)

    def filter_select_legal_move(self, sketchy_moves):
        """
        Filters away the illegal moves and chooses the move with the highest probability
        :return: Move to be made
        """
        sketchy_moves = np.array(sketchy_moves)  # This will be a tensor i guess, problems will happen lmao
        if self.K > self.N:
            if self.K - self.N == 1:    # Arange doesnt handle 0 in range
                legal_moves = np.delete(sketchy_moves, self.N)  # N is technically +1, since arrays index from 0
            else:
                legal_moves = np.delete(sketchy_moves, np.arange(self.N, self.K))  # N is technically +1, since arrays index from 0
        else:   # All actions are possible
            legal_moves = sketchy_moves
        print(legal_moves)
        legal_move = np.argmax(legal_moves)
        return legal_move



    def is_final(self):
        if self.N == 0:
            return True
        else:
            return False

    def reward(self):
        if self.is_final() and self.player == 1:
            return 1
        elif self.is_final() and self.player == 2:
            return -1
        elif not self.is_final():
            return 0

    def get_state(self):
        return self.N
